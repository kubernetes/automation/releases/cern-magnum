#!/bin/bash
# packages cern-magnum chart(s).
#
# CRDs are packaged inside of a separate chart from the rest of the umbrella
# helm chart as we have reached the 2MB limit size limit of kubernetes secrets
# for our helm release.
#
# Installation of `cern-magnum` therefore has the implicit assumption that
# `cern-magnum-crds` has been installed prior.
exit() { echo -e "\n$0:${BASH_LINENO[0]} '$BASH_COMMAND' failed" >&2; }
trap exit ERR
set -eoa pipefail

CERN_MAGNUM_CHART_NAME=${CERN_MAGNUM_CHART_NAME:="cern-magnum"}
CERN_MAGNUM_CRD_CHART_NAME=${CERN_MAGNUM_CRD_CHART_NAME:="cern-magnum-crds"}
PACKAGE_BUILD_DIRECTORY=${PACKAGE_BUILD_DIRECTORY:="build"}

package-crds() {
  rm -f $CERN_MAGNUM_CHART_NAME*.tgz
  rm -rf $PACKAGE_BUILD_DIRECTORY

  helm dependency update

  # package CRDS defined as sub-chart's templates.
  helm -n kube-system template -f values-crds.yaml $CERN_MAGNUM_CHART_NAME . |
    yq e 'select(.kind == "CustomResourceDefinition")' - >crds/generated.yaml

  # package CRDS defined inside of sub-chart's /crds directory.
  ls charts/*.tgz | xargs -i tar xzf {}
  for d in $(cat Chart.yaml | yq '.dependencies[].name'); do
    echo "packaging crds for ${d}"
    if [ -e "${d}/crds/" ]; then
      mkdir -p crds/${d}
      cp ${d}/crds/*yaml crds/${d}/
    fi
    rm -rf ${d}
  done
}

build-cern-magnum-crds-chart() {
  CHART_DIRECTORY=$PACKAGE_BUILD_DIRECTORY/$CERN_MAGNUM_CRD_CHART_NAME
  mkdir -p $CHART_DIRECTORY
  cp -r crds $CHART_DIRECTORY/crds
  cp Chart.yaml $CHART_DIRECTORY
  yq e -i ".name = \"$CERN_MAGNUM_CRD_CHART_NAME\"" $CHART_DIRECTORY/Chart.yaml
  yq e -i "del(.dependencies)" $CHART_DIRECTORY/Chart.yaml
  helm package $CHART_DIRECTORY
}

build-cern-magnum-chart() {
  CHART_DIRECTORY=$PACKAGE_BUILD_DIRECTORY/$CERN_MAGNUM_CHART_NAME
  mkdir -p $CHART_DIRECTORY
  # Copy everything expect /crds as now in separate chart.
  cp -r charts/ $CHART_DIRECTORY/charts/
  cp -r templates/ $CHART_DIRECTORY/templates/
  cp .helmignore Chart.lock Chart.yaml README.md cluster-config.yaml values-crds.yaml values.yaml $CHART_DIRECTORY/
  helm package $CHART_DIRECTORY
}

{
  cd "${0%/*}"
  package-crds
  build-cern-magnum-crds-chart
  build-cern-magnum-chart
}
