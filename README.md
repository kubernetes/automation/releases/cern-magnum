# cern-magnum

This is the umbrella chart behind all the add-ons available for CERN Kubernetes
clusters.

Check the default `values.yaml` for the multiple configuration options
available.

## Chart Repositories

Charts are expected to exist under registry.cern.ch, all dependencies must
refer to our local repository.

There are two main repositories currently in use:
* [stable](https://registry.cern.ch/harbor/projects/1295/helm-charts): upstream dependencies (old stable repo). A [replication rule](https://registry.cern.ch/harbor/replications) `artifact-hub` takes care of keeping them in sync with upstream versions
* [cern](): charts maintained locally, mostly coming from our [cern charts](https://gitlab.cern.ch/helm/charts/cern) repo

## CRD Management

`cern-magnum` is packaged as two separate helm charts:
 * `cern-magnum-crds` contains only the CRDs.
 * `cern-magnum` contains the umbrella helm chart (excluding the CRDs).

 It is assumed that all available CRDs used by `cern-magnum` are available
 prior to installation. i.e. `cern-magnum-crds` must always be installed before
 installing `cern-magnum`.

Packaging CRDs separately is required as for when multiple dependencies with
very large CRDs are enabled in `cern-magnum` installation breaks owning to the
helm release exceeding the 2MB secret limit in Kubernetes.

Hiding this change purely in the CI from a packaging / installation perspective
provides the least intrusive approach (as it does not require restructuring or
maintaining two `values.yaml` [one for crds and another for the umbrella
chart], nor does it require complicated logic around templates or crds to
selectively install or not install, and it allows for CRDs to be managed as
part of helm releases). 

This is intended as a temporary solution until plans to migrate the management
of cern-magnum as an applicationset in argo are planned for and completed later
on in the year (2024).
